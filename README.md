# HopNRocket
<h2>
Description
</h2>
<p>
A Unity demo in the form of a "Flappy Birds" clone that features turrets as opposed to gapped walls. Developed using Unity 5.
</p>

<h2>
Overview
</h2>
<p>
The game has been implemented using sprites from the Metal Slug franchise. Instead of a bird, the player is a
non-descript soldier equipped with a rocket launcher.
</p>
<p>
The player must shoot rockets at the ground in order to jump. If the player comes into contact with the ground,
enemies, enemy projectiles or obstacles, the game is over and the player must restart by pressing a key.
</p>
<p>
The player may also shoot at a forward down angle in order to destroy turrets, which awards additional points.
</p>
<p>
Points accumulate over time, regardless of how many turrets you shoot. So the longer you last, the more points you get.
</p>
<p>
Each jump shot speeds up the scrolling of the world ever so slightly and shooting forward will slow you down a bit,
however I suspect you'll need to do the former far more than the latter.
</p>
<p>
There are additionally some bomb power-ups that the player may pick up by touching. Activating a bomb power will
destroy all the turrets on-screen and reset the world's scroll speed.
</p>
<p>
Please enjoy.
</p>
